#Universidad del Valle de Guatemala
#Hoja de trabajo 5, Algoritmos y estructura de datos
#23/02/2018
#Javier Carpio 17077
#Maria Fernanda Lopez 17160

import simpy
import random


# clase de simulacion
class Simulacion(object):

    # constructor de la clase 
    def __init__(self,env, simTime, name, ram, memProceso, ins, opCpu):
        self.env = env # ambiente de simulacion simpy
        self.simTime = simTime # tiempo de simulacion
        self.name = name # nombre para identificar proceso 
        self.ram = ram # memoria ram disponible
        self.memProceso = memProceso # cantidad de memoria que utilizara el proceso
        self.ins = ins # instucciones de un proceso
        self.opCpu = opCpu #operacions que puede realizar el cpu

        # iniciar la simulacion
        self.action = env.process(self.proceso(env, simTime, name, ram, memProceso, ins, opCpu))

    # simulacion 
    def proceso(self,env, simTime, name, ram, memProceso, ins, opCpu):
        global tiempoTotal
        

        yield env.timeout(simTime)
        print"%s necesita %d de memoria RAM"%(name, memProceso)

        tiempoLlegada = env.now 
        yield RAM.get(memProceso)

        print "%s ha obtenido %d de memoria "%(name, memProceso)
        insC = 0 # instrucciones disponibles por CPU
        

        # cola para las instrucciones del CPU
        while insC < ins:
            with CPU.request() as reqCPU:
                yield reqCPU

                if (ins-insC)>=opCpu:
                    rend = opCpu 

                else:
                    rend=(ins-insC)

                print"%s el CPU ejecutara %d instrucciones "%(name, rend)
                yield env.timeout(rend/opCpu)   

                insC += rend

                print"%s CPU ejecutando (%d/%d) completado"%(name, insC, ins)

            waiting = random.randint(1,2)

            if ((waiting == 1) and (insC<ins)):

                with WAIT.request() as requestIO:
                    yield requestIO
                    yield env.timeout(1)                
                    print "%s esperando para realizar operaciones (I/O)"%(name)

        yield RAM.put(memProceso)
        print "%s completado: retornando %d de memoria RAM "%(name, memProceso)
        tiempoTotal += (env.now - tiempoLlegada)
        


opCpu = 3.0 # instrucciones que realiza el CPU por unidad de tiempo
tiempoTotal = 0 # tiempo total en la simulacion
tiempoDesv = 0


#instancie el ambiente de simulacion el cpu y la ram que es nuestro container (tomado de ejemplo que subio Douglas y documentacion)
environment = simpy.Environment() 
CPU = simpy.Resource (environment, capacity=3) 
RAM = simpy.Container(environment, init=100, capacity=100)
WAIT = simpy.Resource (environment, capacity=2)  
 

random.seed(10)


# creacion de los procesos
for i in range(200):
    simTime = random.expovariate(1.0 / 1)
    instrucciones = random.randint(1,10)
    memProceso = random.randint(1,10)
    c = Simulacion(environment, simTime, 'Proceso %d' % i, RAM, memProceso, instrucciones, opCpu)
environment.run()
print tiempoTotal
